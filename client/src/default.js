import React, { Component } from 'react';
import NodeRSA from 'node-rsa';
import axios from 'axios';
import {
  Container,
  Row,
  Col,
  Input,
  UncontrolledAlert
} from 'reactstrap';

class Default extends Component {
  constructor(props) {
    super(props);
    this.state = {
      processing : false
    }
  }
  
  componentDidMount() {
    this.loadAuctions();
  }

  handleInput(e) {
    switch(e.target.name) {
      case 'username':
        this.setState({username:e.target.value})
        break;
      case 'bcci_address':
        const reader = new FileReader();
        reader.readAsBinaryString(e.target.files[0]);
        reader.onload = () => {
          this.setState({key:reader.result});
        }
        break;
      default : 
        return false;
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    const keyPair = new NodeRSA(this.state.key);
    const user = {
      nickname : this.state.username,
      address : keyPair.exportKey('pkcs8-public-pem')
    }
    this.setState({processing : true});
    axios
    .post('http://localhost:3001/Bidders', user)
    .then(response => {
      if(response.status === 200) {
        this.setState({
          bidder  : response.data
        });
        return this.bidderSignature();
      } else {
        this.setState({
          error : response,
          processing : false
        })
      } 
    })
    .catch(error => {
      this.setState({
        error,
        processing : false
      })
    });
    return false;
  }

  bidderSignature() {
    if(this.state.bidder) {
      const keyPair = new NodeRSA(this.state.key);
      const user = {
        id : this.state.bidder._id,
        nickname : this.state.username,
        address : keyPair.exportKey('pkcs8-public-pem'),
      }
      user.signature = keyPair.sign(JSON.stringify(user), "buffer")
      axios
      .put(`http://localhost:3001/Bidders/${this.state.bidder._id}`, user)
      .then(response => {
        if(response.status === 200) {
          this.setState({
            bidder  : response.data,
            processing : false
          });
        } else {
          this.setState({
            error : response,
            processing : false
          })
        } 
      })
      .catch(error => {
        this.setState({
          error,
          processing : false
        })
      });
    }
  }

  loadAuctions() {
    this.setState({
      auctions : {}
    });
    axios.get('http://localhost:3001/Auctions')
      .then(response => {
        if(response.status === 200){
          this.setState({
            auctions : response.data
          });
        } else {
          this.setState({
            error : response
          });
        }
      })
      .catch(error => {
        this.setState({
          error
        })
      });
  }

  render() {
    return (
      <Container fluid>
        {this.state.error ? <UncontrolledAlert color="danger">
          <strong>{this.state.error.message}</strong>
        </UncontrolledAlert> : null}
        {!this.state.processing && this.state.bidder === undefined ? <Row>
          <Col>
            <form onSubmit={this.handleSubmit.bind(this)}>
                <label htmlFor="username">Username</label>
                <Input type="text" name="username" placeholder="Username" onChange={this.handleInput.bind(this)}/>
                <label htmlFor="bcci_address">Private Key</label>
                <Input type="file" name="bcci_address" placeholder="BCCI Wallet Key" onChange={this.handleInput.bind(this)}/>
                <Input type="submit" name="Create Bidder" />
            </form>
          </Col>
        </Row> : null}        
        <Row>
          <Col>
              <h1>Currency Auctions {this.state.bidder ? <span className="float-right">Welcome {this.state.bidder.nickname}</span> : null}</h1>
          </Col>
        </Row>
        <Row>
          <Col sm={4} md={4}>Auction ID</Col>
          <Col>Title</Col>
        </Row>
        {this.state.auctions ? (Object.keys(this.state.auctions)).map(auction => {
          return <Row key={auction}>
            <Col sm={4} md={4}><a href={`/Auction/${auction}`}>{auction}</a></Col>
            <Col>{this.state.auctions[auction]}</Col>
          </Row>
        }) : null}
      </Container>
    );
  }
}

export default Default;
