import React, { Component } from 'react';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import Default from './default';
import Auction from './auction';
class App extends Component {  
  render() {
    return (
      <Router>
        <Switch>
          <Route component={Default} path="/" exact/>
          <Route component={Auction} path="/Auction/:auctionId" exact/>
        </Switch>
      </Router>
    );
  }
}

export default App;
