import React, { Component } from 'react';
import NodeRSA from 'node-rsa';
import axios from 'axios';
import {
  Container,
  Row,
  Col,
  Input,
  UncontrolledAlert
} from 'reactstrap';

class Auction extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loading : true
    };
  }

  componentWillMount() {      
    axios
        .get(`//localhost:3001/Auction/${this.props.match.params.auctionId}`)
        .then(response => {
            this.setState({
                auction : response.data,
                loading : false
            });
        })
        .catch(error => {
            this.setState({
                error,
                loading : false
            });
        });
  }

  render() {
    return (
      <Container fluid>
        {this.state.error ? <UncontrolledAlert color="danger">
          <strong>{this.state.error.message}</strong>
        </UncontrolledAlert> : null}
        {this.state.loading ? 'Loading ...' : null}
        {this.state.auction ? <Container>
            <Row>
                <Col xs={12} sm={12}>
                
                    <h2 className="text-center">{this.state.auction.title}
                    <span className="float-right"><small>Auction ID : {this.state.auction._id}</small></span></h2>
                    type,
                    bidIncrement, 
                    
                    bidders,
                    
                     duration, , lotSize, starts, created
                </Col>
            </Row>
        </Container> : null}
      </Container>
    );
  }
}

export default Auction;
