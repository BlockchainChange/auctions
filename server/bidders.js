const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const Bidder = require('../db-schema/bidder').bidderModel;

/* /Bidders */
const BiddersRouter = express.Router();

mongoose.Promise = global.Promise;
mongoose
    .connect(
        `mongodb://${config.dbHost}/${config.dbName}`
    ).catch(error => {
        throw new Error(error);
        process.exit(-1);
    });

BiddersRouter.post('/', (request, response) => {
    const { nickname, address } = request.body;

    if(nickname === undefined || address === undefined ) {
        return response.status(500).json({"status" : "Error", "message" : "A unique nickname, BCCI wallet address are required."});
    }

    const bidder = new Bidder({ nickname, address });
    return bidder
        .save()
        .then(dbResponse => response.json(dbResponse))
        .catch(error => response.status(500).json(error));
});

BiddersRouter.put('/:bidderId', (request, response) => {
    const { bidderId } = request.params;
    const { nickname, address, signature } = request.body;
    return Bidder
        .findById(bidderId)
        .then(_dbResponse => {
            if(_dbResponse._id.toString() === bidderId && _dbResponse.nickname === nickname &&  _dbResponse.signature.data.length === 0 ) {
                return Bidder
                    .findByIdAndUpdate(bidderId, {nickname, address, signature }, {new: true})
                    .then(dbResponse => response.json(dbResponse))
                    .catch(error => response.status(500).json(error));
            } else {
                return response.status(500).json({"states" :"Error", "Message" : "Unable to attach signature to bidder.", answers: [_dbResponse._id === bidderId]});
            }
        })
        .catch(error => response.status(500).json(error));
});



module.exports = BiddersRouter;