console.log('Starting BCCI Auction Server.');
const { spawn } = require('child_process');
const child = spawn('node', ['server.js'], {
  detached: true,
  stdio: 'ignore'
});
console.log('Auction server started. PID : ' ,  child.pid);
child.unref();

process.exit();
