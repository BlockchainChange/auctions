const express = require('express');
const Auctions = require('../db-schema/auction').auctionModel;

const AuctionsRouter = express.Router();
/* /Auctions */
    
AuctionsRouter.get('/', (request, response) => {
    return Auctions
        .find()
        .then(dbResponse => {
            let body = {};
            dbResponse.forEach(auction => {
                body[auction._id] = auction.title;
            })
            return response.json(body);
        })
        .catch(error => {
            return response.status(500).send(error);    
        });
});
    
module.exports = AuctionsRouter;
