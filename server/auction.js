const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const Auction = require('../db-schema/auction').auctionModel;
const { spawn } = require('child_process');
const Types = {
    
        "USD" : "United States Dollar",
        "BCCI" : "Blockchain Change Initiative",
        "BTC" : "Bitcoin",
        "LTC" : "Litecoin",
        "OTHER" : "Other"
}
/* /Auction */
const AuctionRouter = express.Router();

mongoose.Promise = global.Promise;
mongoose
    .connect(
        `mongodb://${config.dbHost}/${config.dbName}`
    ).catch(error => {
        throw new Error(error);
        process.exit(-1);
    });

AuctionRouter.get('/:auction', (request, response) => {
    const { auction } = request.params;
    return Auction
        .findById(auction)
        .then(dbResponse => {
            response.send(dbResponse);
        })
        .catch(error => {
            return response.status(500).json(error);
        });
});

AuctionRouter.post('/', (request, response) => {
    let {
        title,
        type,
        lotSize,
        bidIncrement,
        starts,
        duration
    } = request.body;    
    
    if (title !== undefined && title.length < 7 || title.length > 101) {
        return response.status(500).json({ "status" : "Error", "message" : "The title is required and must be a) more than 7 characters b) no more than 100 characters."});
    }

    if(type === undefined || (type !== undefined && !Types.hasOwnProperty(type))) {
        return response.status(500).json({ "status" : "Error", "message" : "The type is required and must be USD, BTC, LTC, BCCI, or OTHER"});
    }

    if(lotSize === undefined) {
        return response.status(500).json({ "status" : "Error", "message" : "The lot size is how many are you selling "});
    }
    
    if(bidIncrement === undefined) {
        return response.status(500).json({ "status" : "Error", "message" : "The bid increment is required. This is the lowest amount each bid is required to increment."});
    }
    /*
    if(starts === undefined) {
        return response.status(500).json({ "status" : "Error", "message" : "The start time is required. This timestamp to start the auction is used inconjunction with the duration to control the the auction timing."});
    }
    */
    starts = (Date.now()) + (15 * 60 * 1000);

    let auction = {
        title,
        type,
        lotSize,
        bidIncrement,
        starts,
        created : Date.now()
    }

    const newAuction = new Auction(auction);
    newAuction
        .save()
        .then( _newAuction => {
            const child = spawn('node', [ 'auctioneer.js', _newAuction._id ], { detached: true });
            child
                .unref();
            return response.json({statue:"Success", _newAuction, auction});
        })
        .catch( error => 
            response
                .status(500)
                .send(error)
        );   
    
});

module.exports = AuctionRouter;