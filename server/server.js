const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const server = express();
const router = require('./router');
const corsWhiteListed = (req, callback) => { callback(null, {origin:true}); };
server.use(bodyParser.json(), cors(corsWhiteListed));
server.use('/', router);
server.listen(3001, () => {
    console.log('Server running.');
});