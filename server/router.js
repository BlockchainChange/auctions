const express = require('express');
const AuctionsRouter = require('./auctions');
const AuctionRouter = require('./auction');
const BiddersRouter = require('./bidders');
const BidRouter = require('./bid');
const mainRouter = express.Router();

mainRouter.get('/', ( request, response ) => {
    response.json({ "status":"success", "message":"BCCI Auctions API" });
});
mainRouter.use('/Auctions', AuctionsRouter);
mainRouter.use('/Auction', AuctionRouter);
mainRouter.use('/Bidders', BiddersRouter);
mainRouter.use('/Bid', BidRouter);
    
module.exports = mainRouter;
