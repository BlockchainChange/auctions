const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
/* /bid */
const BidRouter = express.Router();

mongoose.Promise = global.Promise;
mongoose
    .connect(
        `mongodb://${config.dbHost}/${config.dbName}`
    ).catch(error => {
        throw new Error(error);
        process.exit(-1);
    });

BidRouter.post('/', (request, response) => {
    const {
        key,
        signature,
        amount,
        type,
        bidder
    } = request.body;

    response.send('<h1>Bid Router</h1>');
});

module.exports = BidRouter;