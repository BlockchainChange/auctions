const mongoose = require('../server/node_modules/mongoose');
const config = require('../server/config');
const Schema = mongoose.Schema;
const Bidder = require('./bidder').bidderSchema;
const Bid = require('./bid').bidSchema;

mongoose.Promise = global.Promise;
mongoose
    .connect(`mongodb://${config.dbHost}/${config.dbName}`)
    .catch((error => {
        throw new Error(error);
        process.exit(-1);
    }));

const auctionSchema = Schema({        
    title : { 
        type : String, 
        max : 100,
        unique : true
    },
    type: { type: String, enum: [ "USD","BCCI","BTC","LTC", "OTHER" ], default: "BCCI" },
    lotSize : Number,
    bidIncrement : { 
        type : Number,
        default : 0.01
    },
    bidders : [{ type: Schema.Types.ObjectId, ref:"bidder"}],
    created : Date,
    starts : Date,
    highestBidder : { type: Schema.Types.ObjectId, ref:"bidder"},
    highestBid : { type: Schema.Types.ObjectId, ref:"bid"}
});

auctionModel = mongoose.model('auction', auctionSchema);
module.exports = { auctionModel, auctionSchema }; 
