const mongoose = require('../server/node_modules/mongoose');
const config = require('../server/config');
const Bid = require('./bid').bidSchema;
const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;
mongoose
    .connect(
        `mongodb://${config.dbHost}/${config.dbName}`
    ).catch(error => {
        throw new Error(error);
        process.exit(-1);
    });

const bidderSchema = Schema({
    nickname : {type:String, unique:true, required:true},
    address : [String],
    signature : {
        type: { type: String, enum : ['Buffer'], default: 'Buffer' },
        data:[ String ]
    }
});

const bidderModel = mongoose.model('bidder', bidderSchema);
module.exports = { bidderModel, bidderSchema };
