const mongoose = require('../server/node_modules/mongoose');
const config = require('../server/config');
const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;
mongoose
    .connect(
        `mongodb://${config.dbHost}/${config.dbName}`
    ).catch(error => {
        throw new Error(error);
        process.exit(-1);
    });

const bidSchema = Schema({
    key : String,
    signature :{
        type: { type: String, enum : ['Buffer'], default: 'Buffer', required:true },
        data:[ String ]
    },
    amount : Number,
    type: { 
        type : String,
        enum : [ "USD", "BCCI", "BTC", "LTC" ], 
        default : "BCCI"
    },
    bidder : { type: Schema.Types.ObjectId, ref:"bidder"}
});

const bidModel = mongoose.model('bid', bidSchema);

module.exports = { bidSchema, bidModel };
